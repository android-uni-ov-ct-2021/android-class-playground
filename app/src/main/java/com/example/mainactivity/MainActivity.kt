package com.example.mainactivity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        Log.e("tag", "onCreate")

        val button: Button = findViewById(R.id.btn_go_to_activity_two)
        button.setOnClickListener { goToActivityTwo() }
    }

    override fun onStart() {
        super.onStart()
        Log.e("tag", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.e("tag", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.e("tag", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.e("tag", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("tag", "onDestroy")
    }

    fun goToActivityTwo() {
        val intent = Intent(this, Step2Activity::class.java)
        startActivity(intent)
    }
}