package com.example.mainactivity;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class SecondFragment extends Fragment {

    public static final String TAG = "TAG_SECOND_FRAGMENT";

    public SecondFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e("tag", "Second fragment onCreate");
    }

    public static SecondFragment newInstance() {

        Bundle args = new Bundle();

        SecondFragment fragment = new SecondFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        Log.e("tag", "Second fragment onAttach");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.e("tag", "Second fragment onDetach");
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_second, container, false);
        Log.e("tag", "Second fragment onCreateView");
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.e("tag", "Second fragment onStart");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.e("tag", "Second fragment onResume");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.e("tag", "Second fragment onPause");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.e("tag", "Second fragment onStop");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("tag", "Second fragment onDestroy");
    }


}
