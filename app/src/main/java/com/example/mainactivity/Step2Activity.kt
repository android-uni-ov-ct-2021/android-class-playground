                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    package com.example.mainactivity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class Step2Activity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_step2)
        onAddFirstFragment()
        onAddSecondFragment()
        Log.e("tag", "onCreate")
    }

    private fun onAddFirstFragment() {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction() //add -> adauga peste
        transaction.add(
            R.id.frame_layout_container,
            FirstFragment.newInstance("I AM", "First fragment"),
            FirstFragment.TAG
        )
        transaction.addToBackStack(FirstFragment.TAG)
        transaction.commit()
    }

    private fun onAddSecondFragment() {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction() //add -> adauga peste
        transaction.add(
            R.id.frame_layout_container,
            SecondFragment.newInstance(),
            SecondFragment.TAG
        )
        transaction.addToBackStack(SecondFragment.TAG)
        transaction.commit()
    }

    override fun onStart() {
        super.onStart()
        Log.e("tag", "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.e("tag", "onResume")
    }

    override fun onPause() {
        super.onPause()
        Log.e("tag", "onPause")
    }

    override fun onStop() {
        super.onStop()
        Log.e("tag", "onStop")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.e("tag", "onDestroy")
    }
}